package mx.edu.uttijuana.nfl;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import mx.edu.uttijuana.nfl.Modelos.Child;
import mx.edu.uttijuana.nfl.Modelos.Group;

public class ExpandableGroupAdapter extends BaseExpandableListAdapter {

    //Attributes
    private ArrayList<Group> groups;
    private Context context;

    //Constructor
    public ExpandableGroupAdapter(Context context, ArrayList<Group> groups) {
        this.context = context;
        this.groups = groups;
    }

    @Override
    public int getGroupCount() {

        return this.groups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.groups.get(groupPosition).children.size();
    }

    @Override
    public Object getGroup(int groupPosition) {

        return groups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return groups.get(groupPosition).children.get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {

        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {

        return childPosition;
    }

    @Override
    public boolean hasStableIds() {

        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(this.context);
        View view = inflater.inflate(R.layout.item_group, null);

        //Controls
        ImageView imageLogo = view.findViewById(R.id.logoConference);
        TextView textConference = view.findViewById(R.id.viewConference);

        Group g = this.groups.get(groupPosition);
        //List values to controls
        int r = view.getResources().getIdentifier(g.getLogo(), "drawable", this.context.getPackageName());
        imageLogo.setImageResource(r);
        textConference.setText(g.getName());
        return view;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(this.context);
        View view = inflater.inflate(R.layout.item_child, null);

        //Controls
        ImageView imageLogo = view.findViewById(R.id.viewLogo);
        TextView textCity = view.findViewById(R.id.viewCity);
        TextView textTeam = view.findViewById(R.id.viewTeam);

        Child c = this.groups.get(groupPosition).children.get(childPosition);
        //List values to controls
        int r = view.getResources().getIdentifier(c.getLogo(), "drawable", this.context.getPackageName());
        imageLogo.setImageResource(r);
        textCity.setText(c.getCity());
        textTeam.setText(c.getTeam());
        //Return
        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
