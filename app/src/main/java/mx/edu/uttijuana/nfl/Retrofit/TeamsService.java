package mx.edu.uttijuana.nfl.Retrofit;

import java.util.ArrayList;

import mx.edu.uttijuana.nfl.Modelos.Group;
import retrofit2.Call;
import retrofit2.http.GET;

public interface TeamsService {

    @GET("teams")
    Call<ArrayList<Group>> getTeams();
}
