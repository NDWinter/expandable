package mx.edu.uttijuana.nfl.Modelos;

import android.media.Image;
import android.widget.ImageView;

public class Child {

    //Attributes
    public String city;
    public String team;
    public String logo;

    //Getters and setters
    public String getCity() { return city; }
    public void setCity(String city) { this.city = city; }

    public String getTeam() { return team; }
    public void setTeam(String team) { this.team = team; }

    public String getLogo() { return logo; }
    public void setLogo(String logo) { this.logo = logo; }

    //Constructor
    public Child(String logo, String city, String team) {
        this.city = city;
        this.team = team;
        this.logo = logo;
    }

    public Child() {
        this.city = "";
        this.team = "";
        this.logo = "";
    }
}
