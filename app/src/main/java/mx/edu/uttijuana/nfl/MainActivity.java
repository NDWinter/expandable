package mx.edu.uttijuana.nfl;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import java.util.ArrayList;

import mx.edu.uttijuana.nfl.Modelos.Group;
import mx.edu.uttijuana.nfl.Retrofit.RetrofitFactory;
import mx.edu.uttijuana.nfl.Retrofit.TeamsService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    ExpandableGroupAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getNetworkDate();
    }

    private void getNetworkDate() {
        final ExpandableListView expandableListView = findViewById(R.id.expListView);

        Retrofit retrofit = RetrofitFactory.getRetrofit();
        TeamsService service = retrofit.create(TeamsService.class);
        Call<ArrayList<Group>> call = service.getTeams();

        call.enqueue(new Callback<ArrayList<Group>>() {
            @Override
            public void onResponse(Call<ArrayList<Group>> call, Response<ArrayList<Group>> response) {

                adapter = new ExpandableGroupAdapter(MainActivity.this, response.body());
                expandableListView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<ArrayList<Group>> call, Throwable t) {
                String xD = t.getMessage();
            }
        });


    }
}
