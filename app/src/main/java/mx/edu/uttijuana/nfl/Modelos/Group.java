package mx.edu.uttijuana.nfl.Modelos;

import java.util.ArrayList;

public class Group {
    // Attributes.
    public String name;
    public String logo;
    public ArrayList<Child> children = new ArrayList<Child>();

    // Getters and setters.
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }
    public String getLogo() { return logo; }
    public void setLogo(String logo) { this.logo = logo; }
    public ArrayList<Child> getChildren() { return children; }
    public void setChildren(ArrayList<Child> children) { this.children = children; }

    // Constructor.
    public Group(String logo, String name, ArrayList<Child> children) {
        this.name = name;
        this.logo = logo;
        this.children = children;
    }
    public Group() {
        this.name = "";
        this.logo = "";
        this.children = null;
    }

    //Methods
    public static ArrayList<Group> getAll() {

        ArrayList<Group> lista = new ArrayList<Group>();

        ArrayList<Child> children = new ArrayList<Child>();

        //add child to children
        children.add(new Child("jaguars", "Jacksonville","Jaguars"));
        children.add(new Child("bengals", "Cincinnati","Bengals"));
        children.add(new Child("broncos", "Denver","Broncos"));
        children.add(new Child("bills", "Buffalo","Bills"));
        children.add(new Child("colts", "Indianapolis","Colts"));
        children.add(new Child("browns", "Cleveland","Browns"));
        children.add(new Child("texans", "Houston","Texans"));
        children.add(new Child("ravens", "Baltimore","Ravens"));

        //add to group
        Group a = new Group("afc", "American Football Conference", children);
        lista.add(a);

        ArrayList<Child> childrens = new ArrayList<Child>();

        //add child to children
        childrens.add(new Child("panthers", "Charlotte","Panthers"));
        childrens.add(new Child("steelers", "Pittsburgh","Steelers"));
        childrens.add(new Child("saints", "Nueva Orleans","Saints"));
        childrens.add(new Child("jets", "East Rutherford","Jets"));
        childrens.add(new Child("redskins", "Washington","Redskins"));
        childrens.add(new Child("lions","Detroit","Lions"));
        childrens.add(new Child("rams","Los Angeles","Rams"));
        childrens.add(new Child("cowboys","Dallas","Cowboys"));
        //add to group
        Group b = new Group("nfc", "National Football Conference", childrens);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);
        lista.add(b);


        // Return lista
        return lista;
    }
}
